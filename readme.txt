=== Better Admin UI ===
Author: Ian Reid Langevin
Tags: admin, UI, UX, admin panel
Requires at least: 4.5
Tested up to: 5.1.1
License: GPLv3
License URI: https://www.gnu.org/licenses/gpl-3.0.html


== Description ==

A super light plugin that add an elegant and modern look to the Wordpress admin. No javascript, database tables or php functions are added. It's only a small CSS stylesheet.

Add a better look to majors plugins likes ACF - Advanced Custom Fields, WPML, Gravity Forms, Yoast and SEO Press.


= Automatic installation =

Automatic installation is the easiest option as WordPress handles the file transfers itself and you don’t need to leave your web browser. To
do an automatic install of, log in to your WordPress dashboard, navigate to the Plugins menu and click Add New.

In the search field type “Better Admin UI” and click Search Plugins. Once you’ve found the plugin, you can install it by simply clicking "Install Now".


= Manual installation =

The manual installation method involves downloading the plugin and uploading it to your web server via your favorite FTP application. The WordPress codex contains [instructions on how to do this here](http://codex.wordpress.org/Managing_Plugins#Manual_Plugin_Installation).


= Updating =

Automatic updates should work like a charm.


== Screenshots ==

== Changelog ==

== Upgrade Notice ==