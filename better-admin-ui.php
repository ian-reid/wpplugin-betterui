<?php
/*------------------------------------------------------------------------------
Plugin Name: Better Admin UI
Plugin URI:
Description: With only a super light CSS stylesheet, this plugin gives a better UI and a modern twist to the Wordpress backend.
Author: Ian Reid Langevin
Author URI: https://ianreidlangevin.com
Programmer: Ian Reid Langevin
Version:1.1.1

------------------------------------------------------------------------------*/


/*
* auto update from Gitlab private mysqli_repo
*/

require 'plugin-update-checker/plugin-update-checker.php';
$myUpdateChecker = Puc_v4_Factory::buildUpdateChecker(
	'https://gitlab.com/ian-reid/wpplugin-betterui/',
	__FILE__,
	'better-admin-ui.php'
);

//Optional: If you're using a private repository, specify the access token like this:
//$myUpdateChecker->setAuthentication('key');

//Optional: Set the branch that contains the stable release.
$myUpdateChecker->setBranch('master');



/*
* enqueue plugin
*/

function BetterUI_admin_theme_style() {
    wp_enqueue_style('my-admin-theme', plugins_url('dist/admin-style.min.css', __FILE__));
}
add_action('admin_enqueue_scripts', 'BetterUI_admin_theme_style');
add_action('login_enqueue_scripts', 'BetterUI_admin_theme_style');


?>
